\documentclass[dvipsnames,10pt,aspectratio=169]{beamer}

\usetheme[progressbar=foot]{metropolis}

\usepackage[english] {babel}
\input{macros}
\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{commons, multirow, xfrac, xcolor}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,positioning}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing}

\title{Separating ABPs and some Structured Formulas in the Non-Commutative Setting}
%\subtitle{}

\author{\textbf{Prerona Chatterjee}}
\institute{Tata Institute of Fundamental Research, Mumbai}
\date{\today}
% \titlegraphic{\hfill\includegraphics[height=1.5cm]{logo.pdf}}

\begin{document}
\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 2em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\begin{frame}{The Non-Commutative Setting}
	\[
		f(x,y) = (x+y) \times (x+y) \text{\pause} = x^2 + xy + yx + y^2  
	\]\pause
	
	\vspace{.5em}
	Natural restriction with long line of work, beginning with [\textbf{Hya '77}], [\textbf{Nis '91}].\\ \pause

	\vspace{.5em}
	Since it is a restricted setting, it is possibly easier to prove lower bounds here.\pause
	\begin{itemize}
		\item $\Det{n}(\vecx)$ has \textcolor{MidnightBlue}{small} $(\poly(n))$ complexity in the commutative setting but is expected to have \textcolor{ForestGreen}{very large} $(2^{\Omega(n)})$ complexity in this setting (\textbf{AS '18}).\pause
		\item \textcolor{ForestGreen}{Exponential} lower bounds known for certain models via exact characterisation (\textbf{Nis '91}). Best lower bound for corresponding models in the commutative setting is \textcolor{MidnightBlue}{quadratic} (\textbf{Kal '85, \textcolor{BrickRed}{C}KSV '20}).\pause
		\item Hardness Amplification is known (\textbf{CILM '18}).
	\end{itemize}
\end{frame}

\begin{frame}{Algebraic Branching Programs}
	\begin{center}
		\begin{tikzpicture}[thick, node distance=2cm]
			\node[circle, draw=black] (start) {$s$};
			\node[circle, draw=black] (l11) at ($(start)+(2,1)$) {};
			\node[circle, draw=black] (l12) at ($(start)+(2,-1)$) {};
			\node[circle, draw=black] (l21) at ($(start)+(4,0)$) {};
			\node[circle, draw=black] (l31) at ($(start)+(6,1)$) {};
			\node[circle, draw=black] (l32) at ($(start)+(6,-1)$) {};
			\node[circle, draw=black] (l41) at ($(start)+(8,1)$) {};
			\node[circle, draw=black] (l42) at ($(start)+(8,-1)$) {};
			\node[circle, draw=black] (end) at ($(start)+(10,0)$) {t};
			
			\draw[->](start) -- (l11);
			\only<3>{\textcolor{red}{\draw[->](start) -- (l11);}}
			\draw[->](start) -- (l12);
			\draw[->](l11) -- (l21);
			\only<3>{\textcolor{red}{\draw[->](l11) -- (l21);}}
			\draw[->](l12) -- (l21);
			\draw[->](l21) -- (l31);
			\only<3>{\textcolor{red}{\draw[->](l21) -- (l31);}}
			\draw[->](l21) -- (l32);
			\draw[->](l31) -- (l42);
			\only<3>{\textcolor{red}{\draw[->](l31) -- (l42);}}
			\draw[->](l32) -- (l41);
			\draw[->](l32) -- (l42);
			\draw[->](l41) -- (end);
			\draw[->](l42) -- (end);
			\only<3>{\textcolor{red}{\draw[->](l42) -- (end);}}
		\end{tikzpicture}
	\end{center}
	\vspace{.75em} \pause
	\begin{itemize}
		\item \textbf{Label on each edge:} \hspace{1em} A homogeneous linear form in $\set{x_1, x_2, \ldots , x_n}$ \pause
		\item \textbf{Weight of path $p$ = $\mathsf{wt}(p)$:} \hspace{1em} Product of the edge labels on $p$ \pause
		\item \textbf{Polynomial computed by the ABP:} \hspace{1em} $\sum_p \mathsf{wt}(p)$ \pause
		\item \textbf{Size of the ABP:} \hspace{1em} Number of vertices in the ABP\pause
	\end{itemize}
	\vspace{1em}
	For a general polynomial $f$ of degree $d$, \hspace{1em} $f = \mathsf{Hom}_\zero(f) + \mathsf{Hom}_1(f) + \cdots + \mathsf{Hom}_d(f)$.

	\vspace{.75em}
\end{frame}

\begin{frame}{Nisan's Characterisation}
\begin{columns}
	\begin{column}{.55\textwidth}
		\begin{tikzpicture}
			\draw (0,0) rectangle (4,5);
			\node[rotate=90] at (-.5,2.5) {Monomials of degree $i$};
			\node at (2,5.5) {Monomials of degree $d-i$};
			\node[draw, BrickRed] (pt) at (2.75,2.75) { };
			\node (m1) at (2.75,-.5) {$m_2$};
			\node (m2) at (4.5,2.75) {$m_1$};
			\draw (pt) -- (m1);
			\draw (pt) -- (m2);
			\draw[thick,->] (2.75,2.75) .. controls (3,1) and (4,2) .. (4.5,1.5);
			\node at (5.5,1) {$\coeff_{m_1 \cdot m_2}(f)$};
		\end{tikzpicture}
	\end{column}
	\begin{column}{.45\textwidth}
		$f$ is a polynomial of degree $d$.\\
		\vspace{1em}
		For every $1 \leq i \leq d$, consider the matrix $M_f(i)$ described alongside.\\ \pause
		\vspace{1em}
		\textbf{Nisan (1991):} For every $1 \leq i \leq d$,\\
		The \textcolor{ForestGreen}{number of vertices} in the $i$-th layer of the smallest ABP computing $f$ is \textcolor{BrickRed}{equal to} the \textcolor{MidnightBlue}{rank} of $M_f(i)$.\\ \pause
		\vspace{1em}
		If $\mathcal{A}$ is the smallest ABP computing $f$,
		
		\vspace{-.5em} 
		\[
			\size(\abp) = \sum_{i=1}^{d} \rank(M_f(i)).	
		\]
	\end{column}
\end{columns}
\end{frame}

\begin{frame}{Algebraic Formulas}
\begin{columns}
	\begin{column}{.5\textwidth}
		\begin{tikzpicture}[thick, node distance=1cm]
			\draw
			node [name=dummy] {}
			node [sum, right of=dummy] (top) {\suma}
			node [sum] at ($(top)+(-1,-1.5)$) (mult1) {\suma}
			node [sum] at ($(top)+(1,-1.5)$) (mult2) {\proda}
			node [sum] at ($(top)+(-2.25,-3)$) (sum1) {\suma}
			node [sum] at ($(top)+(-.75,-3)$) (sum2) {\proda}
			node [sum] at ($(top)+(0.75,-3)$) (sum3) {\suma}
			node [sum] at ($(top)+(2.25,-3)$) (sum4) {\suma};
			\node (var1) at ($(top)+(-2.75,-4.5)$) {$x_1$};
			\node (var2) at ($(top)+(-2,-4.5)$) {$x_2$};
			\node (var3) at ($(top)+(-1.25,-4.5)$) {$x_2$};
			\node (var4) at ($(top)+(-.5,-4.5)$) {$x_3$};
			\node (var5) at ($(top)+(0.5,-4.5)$) {$\alpha$};
			\node (var6) at ($(top)+(1.25,-4.5)$) {$x_2$};
			\node (var7) at ($(top)+(2,-4.5)$) {$x_1$};
			\node (var8) at ($(top)+(2.75,-4.5)$) {$x_3$};
			
			\draw[->](top) -- ($(top)+(0cm,1cm)$);
			\draw[->](mult1) -- node[left] {$\alpha_1$} (top);
			\draw[->](mult2) -- node[right] {$\alpha_2$} (top);
			\draw[->](sum1) -- (mult1);
			\draw[->](sum2) -- (mult1);
			\draw[->](sum3) -- (mult2);
			\draw[->](sum4) -- (mult2);
			\draw[->](var1) -- (sum1);
			\draw[->](var2) -- (sum1);
			\draw[->](var3) -- (sum2);
			\draw[->](var4) -- (sum2);
			\draw[->](var5) -- (sum3);
			\draw[->](var6) -- (sum3);
			\draw[->](var7) -- (sum4);
			\draw[->](var8) -- (sum4);   
			
			\node at (1.5,.8) {$\mathcal{F}$};
		\end{tikzpicture}
	\end{column} \pause
	\begin{column}{.5\textwidth}
		\[
			\mathsf{VF_{nc} \subseteq VBP_{nc}}
		\]\pause

		\vspace{-1em}

		\begin{center}
			Super-polynomial ABP lower bounds\\
			$\Downarrow$\\
			super-polynomial formula lower bounds.
		\end{center}\pause

		\vspace{.5em}
		\textbf{Exponential Lower Bound [Nisan]:}\\ 
		Any ABP (or formula) computing $\Det{n}(\vecx)$ has size at least $2^{\Omega(n)}$.\pause

		\vspace{.5em}
		\begin{center}
			\textbf{Question:} Is $\mathsf{VF_{nc}} = \mathsf{VBP_{nc}}$?
		\end{center}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}{Abecedarian Polynomials}
	\only<1-5>
	{
		\begin{center}
			Generalises the notion of \emph{ordered polynomials} (defined in \textbf{[HWY11]}).
		\end{center}
	}
	\only<6-8>
	{
		\begin{center}
			Variables in every monomial arranged in non-decreasing order of bucket indices.
		\end{center}
	}
	\uncover<2->
	{
		\only<1-3>
		{
			\[
				\Det{n}(\vecx) = \sum_{\sigma \in S_n} (-1)^{\sgn(\sigma)} x_{1, \sigma(1)} \cdots x_{n, \sigma(n)}
			\]
		}
		\only<4>
		{
			\[
				\Perm{n}(\vecx) = \sum_{\sigma \in S_n} x_{1, \sigma(1)} \cdots x_{n, \sigma(n)}
			\]
		}
		\only<5-6>
		{
			\[
				\chsym{n}{d} (\vecx) = \sum_{1 \leq i_1 \leq \ldots \leq i_d \leq n} x_{i_1} \cdots x_{i_d}
			\]
		}
		\only<7>
		{
			\[
				\esym{n}{d}(\vecx) = \sum_{1 \leq i_1 < \ldots < i_d \leq n} x_{i_1} \cdots x_{i_d}
			\]
		}
		\only<8>
		{
			\[
				f(\vecx) \qquad \xrightarrow[\text{using some fixed } \sigma \in S_n]{\text{Order the monomials}} \qquad f^{(nc)}(\vecx)
			\]
		}
	}
	\uncover<3->
	{
		\begin{table}
			\begin{tabular}{ c | c }
				\hline
				\\ [-1em] 
				\textbf{Buckets} & \textbf{Example} \\
				\hline & \\ [-.75em] \hline
				\\ [-1em]
				$\set{X_i}_{i \in [n]}$ where $X_i = \set{x_{ij}}_{j \in [n]}$ & $\Det{n}(\vecx)$\only<4->{, $\Perm{n}(\vecx)$}\\
				\\ [-1em]
				\hline
				\\ [-1em]
				\uncover<6->{\multirow{2}{*}{$\set{X_i}_{i \in [n]}$ where $X_i = \set{x_i}$}} & \uncover<6->{$\chsym{n}{d}(\vecx)$}\only<7->{, $\esym{n}{d}(\vecx)$}\\
				\\ [-1em]
				& \uncover<8->{Non-Commutative version of any $f \in \F[x_1, \ldots, x_n]$}\\ \hline
			\end{tabular}
		\end{table}
	}
	\only<9>
	{
		\vspace{1em}
		\textbf{Note}:
		\[
			\oesym{n}{d} = \sum_{1 \leq i_1 < \ldots < i_d \leq n} x^{(1)}_{i_1} \cdots x^{(d)}_{i_d}
		\]
	
		is $\abcd$ w.r.t. both $\set{X_k = \set{x^{(k)}_i}_{i \in [n]}}_{k \in [d]}$ as well as $\set{X_i = \set{x^{(k)}_i}_{k \in [d]}}_{i \in [n]}$.
	}
\end{frame}

\begin{frame}{Syntactically Abecedarian Models}
	\textbf{Notation}: Consider $1 \leq a \leq b \leq m+1$ \quad where \quad $m$: size of the bucketing system.\pause
	\begin{itemize}
		\item For any $a \in [m+1]$, \qquad \hspace{1em} $f[a, a)$ is the constant term in $f$.\pause
		\item For $1 \leq a < b \leq m+1$, \quad \hspace{.6em} $f\textcolor{MidnightBlue}{[a},\textcolor{ForestGreen}{b)}$ contains only those monomials of $f$ in which\\
		\qquad \qquad \qquad \qquad \qquad \qquad the first variable is from bucket $X_{\textcolor{MidnightBlue}{a}}$;\\
		\qquad \qquad \qquad \qquad \qquad \qquad last variable is from bucket $X_{\textcolor{ForestGreen}{a}}$ or $X_{\textcolor{ForestGreen}{a+1}}$ or $\ldots$ or $X_{\textcolor{ForestGreen}{b-1}}$.\pause
	\end{itemize}

	\vspace{.5em}
	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{center}
				\textbf{Abecedarian Formulas}

				\vspace{.5em}
				Every vertex $v$ is labelled by a tuple $(a,b)$ \pause
				\[
					f_v \text{ is the polynomial at } v \implies f_v = f_v[a,b)	
				\]

				\vspace{.2em}
			\end{center}\pause
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{center}
				\textbf{Abecedarian ABPs}

				\vspace{.5em}
				Every vertex is labelled by a bucket index\\ \pause
				
				\vspace{.5em}
				$f$ is the polynomial computed between $(u,a)$ and $(v,b) \implies f = f[a,b+1)$. 
			\end{center}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{What was known and what we show}
	\textbf{[LLS '19]:} Any $\mathsf{UPT}$ formula computing $\imm{n}{n}(\vecx)$ must have size $n^{\Omega(\log n)}$.\pause

	\vspace{2em}
	\textbf{Our Main Theorems:}\\
	\begin{enumerate}
		\item There is an explicit $n^2$-variate, degree $d$ polynomial $f_{n,d}(\vecx)$ which is $\abcd$ with respect to a \textcolor{BrickRed}{bucketing system of size $n$} such that \pause
			\begin{itemize}
				\item $f_{n, d}(\vecx)$ can be computed by an \textcolor{MidnightBlue}{$\abcd$ ABP} of \textcolor{MidnightBlue}{polynomial} size;\pause
				\item any \textcolor{ForestGreen}{$\abcd$ formula} computing $f_{n, \log n}(\vecx)$ must have size that is \textcolor{ForestGreen}{super-polynomial} in $n$.\pause
			\end{itemize}
		\item Let $f$ be an $n$-variate $\abcd$ polynomial with respect to a \textcolor{BrickRed}{bucketing system of size $O(\log n)$} that can be computed by an ABP of size $\poly(n)$.\\ \pause 
		A super-polynomial lower bound against $\abcd$ formulas for $f$ would imply that $\mathsf{VF_{nc}} \neq \mathsf{VBP_{nc}}$.
	\end{enumerate}
\end{frame}

\begin{frame}{Possible New Approaches to Solving the General Question}
	Two natural questions that arise at this point: \pause
	\begin{enumerate}
		\item Can any \textcolor{MidnightBlue}{formula} computing an $\abcd$ polynomial be \textcolor{MidnightBlue}{converted} to an $\abcd$ formula \textcolor{MidnightBlue}{without much blow-up} in size, \textcolor{BrickRed}{irrespective of} the \textcolor{ForestGreen}{size of the bucketing system}? \pause
		\item Is there a polynomial \textcolor{BrickRed}{$f$ which is $\abcd$} with respect to a \textcolor{MidnightBlue}{bucketing system} that \textcolor{MidnightBlue}{has small size} such that $f$ \textcolor{ForestGreen}{witnesses a separation} between $\abcd$ formulas and ABPs? \pause
	\end{enumerate}

	\vspace{.5em}
	\begin{center}
		$\boxed{\text{A positive answer to either of these questions would imply that $\mathsf{VBP_{nc}} \neq \mathsf{VF_{nc}}$.}}$
	\end{center}
\end{frame}

\begin{frame}{The Explicit Statement}
	\[
		\lchsym{n}{d}(\vecx) = \sum_{i_\zero=1}^{n} \inparen{\sum_{i_\zero \leq i_1 \leq \ldots \leq i_d \leq n} x_{i_\zero, i_1} \cdot x_{i_1,i_2} \cdots x_{i_{d-1}, i_d}}
	\]\pause

	\vspace{1em}
	\begin{itemize}
		\item Abecedarian with respect to $\setdef{X_i}{1 \leq i \leq n}$ where $X_i = \setdef{x_{ij}}{1 \leq j \leq n}$. \pause
		\item $\lchsym{n}{d}(\vecx)$ can be computed by an $\abcd$ ABP of size $O(nd)$. \pause
		\item Any $\abcd$ formula computing $\lchsym{n}{\log n}(\vecx)$ has size $n^{\Omega(\log \log n)}$.\pause
		\item A super-polynomial lower bound against $\abcd$ formulas for $\lchsym{\log n}{n}(\vecx)$ would imply that $\mathsf{VF_{nc}} \neq \mathsf{VBP_{nc}}$.
	\end{itemize}
\end{frame}

\begin{frame}{The Structured ABP Upper Bound}
	\vspace{-.5em}
	\begin{center}
		\boxed{h_{n,d}(\vecx) = \lchsym{n}{d}(\vecx) = \sum_{i_\zero=1}^{n} \inparen{\sum_{i_\zero \leq i_1 \leq \ldots \leq i_d \leq n} x_{i_\zero, i_1} \cdot x_{i_1,i_2} \cdots x_{i_{d-1}, i_d}}}
	\end{center}\pause

	\vspace{.5em}
	\begin{tikzpicture}[thick, node distance=3cm]
		\node [circle, draw=black] (start) {};

		\node[circle, draw=black] (l11) at ($(start)+(2,1.5)$) {$s_1$};
		\node (l12) at ($(start)+(2,.75)$) {$\vdots$};
		\node (l10) at ($(start)+(2,0)$) {$\vdots$};
		\node (l13) at ($(start)+(2,-.75)$) {$\vdots$};
		\node[circle, draw=black] (l14) at ($(start)+(2,-1.5)$) {$s_n$};
		\node at ($(start)+(2,-2.5)$) {$\zero$};

		\node (l21) at ($(start)+(4,1.5)$) {$\cdots$};
		\node (l21) at ($(start)+(4,.75)$) {$\cdots$};
		\node (l23) at ($(start)+(4,-.75)$) {$\cdots$};
		\node (l24) at ($(start)+(4,-1.5)$) {$\cdots$};

		\node (l31) at ($(start)+(6,1.5)$) {$\vdots$};
		\node[circle, draw=black] (l32) at ($(start)+(6,.75)$) {$i$};
		\node (l30) at ($(start)+(6,0)$) {$\vdots$};
		\node (l33) at ($(start)+(6,-.75)$) {$\vdots$};
		\node (l34) at ($(start)+(6,-1.5)$) {$\vdots$};
		\node at ($(start)+(6,-2.5)$) {$k$};

		\node (l41) at ($(start)+(8,1.5)$) {$\vdots$};
		\node[circle, draw=black] (l42) at ($(start)+(8,.75)$) {$i$};
		\node (l40) at ($(start)+(8,0)$) {$\vdots$};
		\node[circle, draw=black] (l43) at ($(start)+(8,-.75)$) {$j$};
		\node (l44) at ($(start)+(8,-1.5)$) {$\vdots$};
		\node at ($(start)+(8,-2.5)$) {$k+1$};

		\node (l51) at ($(start)+(10,1.5)$) {$\cdots$};
		\node (l52) at ($(start)+(10,.75)$) {$\cdots$};
		\node (l53) at ($(start)+(10,-.75)$) {$\cdots$};
		\node (l54) at ($(start)+(10,-1.5)$) {$\cdots$};

		\node[circle, draw=black] (l61) at ($(start)+(12,1.5)$) {$t_1$};
		\node (l62) at ($(start)+(12,.75)$) {$\vdots$};
		\node (l60) at ($(start)+(12,0)$) {$\vdots$};
		\node (l63) at ($(start)+(12,-.75)$) {$\vdots$};
		\node[circle, draw=black] (l64) at ($(start)+(12,-1.5)$) {$t_n$};
		\node at ($(start)+(12,-2.5)$) {$d$};

		\node[circle, draw=black] (end) at ($(start)+(14,0)$) {};

		\draw[->](l32) -- node[above] {$x_{i,i}$} (l42);
		\draw[->](l32) -- node[right] {$x_{i,j}$} (l43);

		\draw[->](start) -- node[above] {$1$} (l11);
		\draw[->](start) -- node[below] {$1$} (l14);

		\draw[->](l61) -- node[above] {$1$} (end);
		\draw[->](l64) -- node[below] {$1$} (end);
	\end{tikzpicture}
\end{frame}

\begin{frame}{Proof Idea of the Structured Formula Lower Bound}
	Let $\form'$ be an $\abcd$ formula of size $s = O(n^{\epsilon \log \log n})$ computing $h_{n/2,\log n}(\vecx)$.
	\begin{columns}
		\begin{column}{.5\textwidth}
			\begin{enumerate}
				\item \textcolor{ForestGreen}{Homogenise} to get $\form'_1$ of size $\poly(s)$. \pause
				\item Using $\form'_1$ construct a more \textcolor{ForestGreen}{structured} formula $\form'_2$ computing $h_{n/2, \textcolor{MidnightBlue}{\log n}}(\vecx)$, of size $\textcolor{BrickRed}{s'_2} = \poly(s)$. \pause
				\item Construct a \textcolor{ForestGreen}{homogeneous, $\abcd$} formula $\form$ of size $\textcolor{BrickRed}{s} = \poly(n)$, that computes $\chsym{n/2}{\textcolor{MidnightBlue}{\log n}}(\vecx)$. \pause
				\item Show that there is a \textcolor{ForestGreen}{homogeneous $\abcd$} formula of size $(\textcolor{BrickRed}{s \cdot s'_2})$ computing $\chsym{n/2}{\textcolor{MidnightBlue}{\log^2 n}} (\vecx)$. \pause
			\end{enumerate} 
		\end{column}
		\begin{column}{.5\textwidth}
			\begin{enumerate}
				\item[5.] \textcolor{ForestGreen}{Reuse Step 4 repeatedly} at most $O(\sfrac{\log n}{\log \log n})$ times to obtain a homogeneous $\abcd$ formula $\form_1$ of size \textcolor{BrickRed}{$O(n^{c \cdot \epsilon \log n})$}, that computes $\chsym{n/2}{\textcolor{MidnightBlue}{n/2}} (\vecx)$ . \pause
				\item[6.] Using $\form_1$ construct a \textcolor{ForestGreen}{homogeneous multilinear} formula of size \textcolor{BrickRed}{$O(n^{c \cdot \epsilon \log n})$} computing \textcolor{MidnightBlue}{$\esym{n}{n/2}(\vecx)$}. \pause
				\item[7.] Choose $\epsilon$ such that Step 6 \textcolor{ForestGreen}{contradicts the known lower bound} against this model for $\esym{n}{n/2}(\vecx)$. 
			\end{enumerate}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Highlighting the Key Steps} 
	\vspace{-.5em}
	Want to use the lower bound in [HY11] against homogeneous multilinear formulas.\pause

	\vspace{1em}
	\begin{columns}
		\begin{column}{.35\textwidth}
			\textbf{Crucial Observations}
			\begin{itemize}
				\item When degree is small, structural changes can be made on formulas.\pause
				\item $\chsym{n}{d}(\vecx)$ is structured enough to allow \emph{degree amplification} (Step 4).\pause
				\item All the other steps are to facilitate the degree amplification.\pause
			\end{itemize}
		\end{column}
		\begin{column}{.65\textwidth}
			\textbf{A little about Step 1}\\
			\begin{itemize}
				\item Brent's Depth Reduction proof works in the non-commutative setting as well.\pause
				\item This allows Raz's Homogenisation proof to go through in this setting as well.\pause
				\item It also answers a question by Nisan.
				
				\vspace{.25em}
				$D(f)$: Depth Complexity; $F(f)$: Formula Complexity
				\vspace{-.25em}
				\[
					\text{Is } D(f) \leq O(\log F(f))?
				\]
				\vspace{.25em}
				We answer the question in the positive.
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Proof Idea for Converting Formulas into Abecedarian Ones}
	\vspace{-.5em}
	Let $\form$ be a formula of size $s$ computing a polynomial that is $\abcd$ with respect to a bucketing system of size $m$.\pause

	\begin{enumerate}
		\item \textcolor{MidnightBlue}{Convert} the given formula $\form$ \textcolor{MidnightBlue}{into an $\abcd$ circuit} $\ckt$.\pause
		\item \textcolor{ForestGreen}{Unravel} $\ckt$ \textcolor{ForestGreen}{to} get an \textcolor{ForestGreen}{$\abcd$ formula} $\form'$ computing the same polynomial.\pause
	\end{enumerate}

	\vspace{1em}
	\begin{columns}
		\begin{column}{.4\textwidth}
			\textbf{Step 1}
			\begin{itemize}
				\item Make $m^2$ copies in $\ckt$ of each vertex in $\form$: $\set{(a,b)}_{a,b \in [m+1]}$.\pause
				\item \quad \hspace{.05em} $f_v$ is the polynomial at $v$\\
				$\qquad \qquad \qquad \hspace{.1em} \Downarrow$\\
				\hspace{.1em} the polynomial at $(v,(a,b))$\\
				\qquad \qquad \quad is $f_v[a,b)$.\pause
			\end{itemize}
		\end{column}
		\begin{column}{.6\textwidth}
			\textbf{Step 2}\\
			\begin{itemize}
				\item Convert $\ckt$ into a formula $\form'$ by recomputing vertices every time it is reused.\pause
				\item Analyse (similar to Raz) the number of distinct paths from any vertex in $\ckt$ to the root.\pause
				\item When $m = O(\log s)$, the size of $\form'$ remains $\poly(s)$.
			\end{itemize}
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{Other Observations}
	\begin{columns}
		\begin{column}{.4\textwidth}
			\begin{enumerate}
				\item $\mathsf{VP_{nc}} = \mathsf{abcd-VP_{nc}}$.\pause
				\item $\mathsf{VBP_{nc}} = \mathsf{abcd-VBP_{nc}}$.\pause
				\item $\mathsf{abcd-VF_{nc}} \subseteq \mathsf{abcd-VBP_{nc}}$.\pause
				\item $\mathsf{abcd-VBP_{nc}} \subsetneq \mathsf{abcd-VP_{nc}}$.\pause
				\item $f$ is an abcd-polynomial of degree $d$ that is computable by an abcd-ABP of size $s$\\
				$\qquad \qquad \quad \Downarrow$\\
				there is an abcd-formula computing $f$ of size $O(s^{\log d})$.\pause
			\end{enumerate}
		\end{column}
		\begin{column}{.6\textwidth}
			\textbf{Corollaries of the Homogenisation Observation}

			\vspace{.5em}
			$2^{\omega(n)}$ lower bound against homogeneous formulas for $\Det{n}(\vecx)$\pause
			
			\vspace{-2em}
			\[
				\text{or}	
			\]
			$n^{\omega(1)}$ lower bound against homogeneous formulas for $\imm{n}{\log n}(\vecx)$\pause
			
			\vspace{-2em}
			\[
				\text{or}	
			\]
			$n^{\omega(1)}$ lower bound against homogeneous formulas for $\lchsym{n}{\log n}(\vecx)$\pause
			\[
				\hspace{-6em} \implies \qquad \qquad \mathsf{VF_{nc}} \neq \mathsf{VBP_{nc}}
			\]
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}
	\begin{center}
		\textbf{\large{Thank you!}}
	\end{center}
\end{frame}
\end{document}